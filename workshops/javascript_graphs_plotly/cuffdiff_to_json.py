#!/usr/bin/env python

import os
import json
import numpy as np
import pandas as pd

CUFFDIFF_FILE = os.path.join(os.path.dirname(__file__), "../../data/GSE86939_Vehicle_DEHP.gene_exp.diff")
VOLCANO_X_MIN = -4
VOLCANO_X_MAX = 4
DO_FILTER = True

def cuffdiff_df_to_volcano(df):
    FOLD_CHANGE = 'log2(fold_change)'
    P_VALUE = "p_value"

    if DO_FILTER:
        P_MAX_CUT_OFF = 0.9
        FOLD_CHANGE_MIN_CUT_OFF = 0.1

        p_cut_off_mask = df[P_VALUE] < P_MAX_CUT_OFF
        fold_change_cut_off_mask = df[FOLD_CHANGE].abs() > FOLD_CHANGE_MIN_CUT_OFF

        print "Orig: %d" % len(df)
        df = df[p_cut_off_mask & fold_change_cut_off_mask]
        print "Filtered: %d" % len(df)
    
    x = df[FOLD_CHANGE]
    y = -np.log10(df[P_VALUE])

    labels = df.index.tolist()

    scatter_plot = {"x" : x.values.tolist(),
                    "y" : y.values.tolist(),
                    "mode" : "markers",
                    "type" : "scatter",
                    "marker" : {'color' : 'rgba(255, 0, 0, .5)'},
                    "text" : labels}

    significant = -np.log10(0.05)
    h_line = {"x" : [VOLCANO_X_MIN, VOLCANO_X_MAX],
              "y" : [significant, significant],
              "mode" : 'lines',
              "name" : 'p = 0.05',
              "line" : {"dash" : 'dot',
                        "color" : "purple"}}
        
    return [scatter_plot, h_line]


def main():
	df = pd.read_table(CUFFDIFF_FILE, sep='\t', index_col='gene')
	data = cuffdiff_df_to_volcano(df)
	layout = {"xaxis" : {	"title" : "log2 fold change",
			     	"range" : [VOLCANO_X_MIN, VOLCANO_X_MAX],
				"autorange" : False,
				'zeroline': False,
                                "showline" : False},
		  "yaxis" : {	"title" : "-log10(p-value)",
				'rangemode': 'nonnegative',
				'zeroline': False,
				#"autorange" : False,
				"showline" : False}}

	with open("volcano_data.js", "w") as f:
		f.write("var volcano_data = %s;\n" % json.dumps(data))
		f.write("var volcano_layout = %s;\n" % json.dumps(layout))


if __name__ == '__main__':
	main();

